﻿/*
 * Takes as Input - Phone number that is valid or invalid.
 * Output - Removes all symbols and returns a valid phone number with a '+' if it is with country code and 
 *          without a '+' otherwise. i.e. 12 digit vs 10 digit phone number.
 * If input contains alphabets it is an invalid input. 
 * */

using System;

namespace PhoneNumberNormalizer
{
    class NumberNormalizer
    {
        public string Normalized(string input)
        {
            char[] inputArr = input.ToCharArray();  // String input to char array
            string newPhone = null;
            string symbols = null;

            for (int i = 0; i < inputArr.Length; i++)
            {
                if (char.IsDigit(inputArr[i]))    //Numbers are separated from the symbols
                {
                    newPhone += inputArr[i];
                }
                else
                {
                    symbols += inputArr[i];
                }
            }

            int count = 0;
            /*If symbols contains any alphabets counter is incremented and if counter is not zero 
             * then the input is considered invalid.*/
            foreach (char c in symbols)
                if (char.IsLetter(c))
                {
                    count += 1;
                }

            string normalized = null;

            if (count != 0)
            {
                normalized = "Invalid Number";
                return normalized;
            }
            else
            {
                if (newPhone.Length == 12)
                {
                    normalized = '+' + newPhone;
                    return normalized;
                }
                else
                {
                    normalized = newPhone;
                    return normalized;
                }
            }
        }

        static void Main(string[] args)
        {
            NumberNormalizer norm = new NumberNormalizer();
            string input = "+43$%$%  )(8923923923";
            string normalized = norm.Normalized(input);
            Console.WriteLine(normalized);
        }
    }
}

